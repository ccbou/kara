<?php

namespace App\Repository;

use App\Entity\Song;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Song|null find($id, $lockMode = null, $lockVersion = null)
 * @method Song|null findOneBy(array $criteria, array $orderBy = null)
 * @method Song[]    findAll()
 * @method Song[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SongRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Song::class);
    }

    // /**
    //  * @return Song[] Returns an array of Song objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @return Song
     */
    public function findOneById($value): ?Song
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.id = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // Get random song for the karaoke
    /**
     * @return Song
     */
    public function findRandomSong()
    {
        return $this->findRandomSongsByANumber(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // Get random songs for the supprting songs
    /**
     * @return Song[]
     */
    public function findHomepageRandomSongs()
    {
        return $this->findRandomSongsByANumber(3)
            ->getQuery()
            ->getResult();
    }

    // Random Blindtest songs
    public function findRandomBlindtestSongs()
    {
        return $this->findRandomSongsByANumber(10)
            ->getQuery()
            ->getResult();
    }

    // Get the 10 last songs for the right part of the homepage
    /**
     * @return Song[]
     */
    public function findTenLatestSongs()
    {
        return $this->addIsPublishedQueryBuilder()
            ->orderBy('s.publishedAt', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    // Get all the songs for the admin song list
    /**
     * @return Song[]
     */
    public function findAllSongsOrderedByCreationDate()
    {
        return $this->createQueryBuilder('s')
            ->orderBy('s.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    //The 'public' songs list
    /**
     * @return Song[]
     */
    public function findAllSongsPublishedByAlphabet()
    {
        return $this->addIsPublishedQueryBuilder()
            ->orderBy('s.title', 'ASC')
            // This join and the addSelect reduct the number of DB queries.
            ->leftJoin('s.genres', 'g')
            ->addSelect('g')
            ->getQuery()
            ->getResult();
    }

    // All Songs By Artist
    /**
     * @return Song[]
     */
    public function findAllSongsByArtist($artist)
    {
        return $this->addIsPublishedQueryBuilder()
            ->andWhere('s.artist = :artist')
            ->setParameter('artist', $artist)
            ->getQuery()
            ->getResult();
    }

    // All Songs By Genre
    /**
     * 
     * @return Song[]
     */
    public function findAllSongsByGenre($genre)
    {
        return $this->addIsPublishedQueryBuilder()
            ->leftJoin('s.genres', 'g')
            ->addSelect('g')
            ->andWhere('g.name = :name')
            ->setParameter('name', $genre)
            ->getQuery()
            ->getResult();
    }


    // For RAND() function, we download beberlei/doctrineextensions
    /**
     * @param int $number
     */
    private function findRandomSongsByANumber($number)
    {
        return $this->addIsPublishedQueryBuilder()
            ->addSelect('RAND() as HIDDEN rand')
            ->orderBy('rand')
            ->setMaxResults($number);
    }

    private function addIsPublishedQueryBuilder(QueryBuilder $qb = null)
    {
        return $this->getOrCreateQueryBuilder($qb)
            ->andWhere('s.publishedAt IS NOT NULL');
    }

    private function getOrCreateQueryBuilder(QueryBuilder $qb = null)
    {
        return $qb ?: $this->createQueryBuilder('s');
    }
}
