<?php

namespace App\Form;

use App\Entity\Song;
use App\Entity\Genre;
use App\Repository\GenreRepository;
use Symfony\Component\Form\AbstractType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SongFormType extends AbstractType
{
    private $gr;

    public function __construct(GenreRepository $gr)
    {
        $this->gr = $gr;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('artist')
            ->add('videoLink')
            ->add('imageFile', VichFileType::class, [
                'download_label' => false,
                'required' => false,
            ])
            ->add('audioFile', VichFileType::class, [
                'download_label' => false,
                'required' => false
            ])
            ->add('genres', EntityType::class, [
                'class' => Genre::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'choices' => $this->gr->findAllGenresByAlphabet()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Song::class,
        ]);
    }
}
