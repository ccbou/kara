<?php

namespace App\Form;

use App\Entity\Song;
use App\Entity\User;
use App\Entity\Playlist;
use App\Repository\PlaylistRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PlaylistSongFormType extends AbstractType
{
    private $pr;
    private $user;

    public function __construct(TokenStorageInterface $tokenStorage, PlaylistRepository $pr = null)
    {
        $this->pr = $pr;
        $this->user = $tokenStorage->getToken()->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('playlists', EntityType::class, [
                'class' => Playlist::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'choices' => $this->pr->findPlaylistsByUser($this->user)
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Song::class,
        ]);
    }
}
