<?php

namespace App\Security\Voter;

use App\Entity\Song;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SongVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['EDIT', 'DELETE', 'CUT'])
            && $subject instanceof Song;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Song $subject */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'EDIT':
                // this is an admin
                if ($this->security->isGranted("ROLE_ADMIN")) {
                    return true;
                }
                return false;
                break;
            case 'DELETE':
                if ($this->security->isGranted("ROLE_ADMIN")) {
                    return true;
                }
                return false;
                break;
            case 'CUT':
                if ($this->security->isGranted("ROLE_ADMIN")) {
                    return true;
                }
                return false;
                break;
            case 'POST_VIEW':
                // logic to determine if the user can VIEW
                // return true or false
                break;
        }

        return false;
    }
}
