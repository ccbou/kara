<?php

namespace App\Security\Voter;

use App\Entity\Playlist;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class PlaylistVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['EDIT', 'SHOW', 'DELETE', 'KARAOKE_PLAYLIST', 'REMOVE_SONG_FROM_PLAYLIST', 'POST_VIEW'])
            && $subject instanceof \App\Entity\Playlist;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /**@var Playlist $subject */
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'EDIT':
                if ($subject->getUser() == $user) {
                    return true;
                }
                return false;
                break;
            case 'SHOW':
                if ($subject->getUser() == $user) {
                    return true;
                }
                return false;
                break;
            case 'DELETE':
                if ($subject->getUser() == $user) {
                    return true;
                }
                return false;
                break;
            case 'KARAOKE_PLAYLIST':
                if ($subject->getUser() == $user) {
                    return true;
                }
                return false;
                break;
            case 'REMOVE_SONG_FROM_PLAYLIST':
                if ($subject->getUser() == $user) {
                    return true;
                }
                return false;
                break;
            case 'POST_VIEW':
                // logic to determine if the user can VIEW
                // return true or false
                break;
        }

        return false;
    }
}
