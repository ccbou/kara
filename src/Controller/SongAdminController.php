<?php

namespace App\Controller;

use App\Entity\Song;
use App\Form\SongFormType;
use falahati\PHPMP3\MpegAudio;
use App\Repository\SongRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class SongAdminController extends AbstractController
{
    /**
     * @Route("/admin/song/list", name="admin_song_list")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(SongRepository $sr, PaginationService $paginationService, Request $request)
    {
        $datas = $sr->findAllSongsOrderedByCreationDate();
        $songs = $paginationService->pagination($datas, 10, 1);

        return $this->render('song_admin/list.html.twig', [
            'songs' => $songs,
        ]);
    }

    /**
     * @Route("/admin/song/{slug}/edit", name="admin_song_edit")
     * @IsGranted("EDIT", subject="song")
     */
    public function edit(Song $song, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(SongFormType::class, $song);
        $form->handleRequest($request);

        //dd($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $song->setUpdatedAt(new \DateTime());
            $em->flush();

            $this->addFlash('success', 'The song - ' . $song->getTitle() . ' - has been updated.');

            return $this->redirectToRoute('admin_song_list');
        }
        return $this->render('song_admin/edit.html.twig', [
            'songForm' => $form->createView(),
            'song' => $song
        ]);
    }

    /**
     * @Route("/admin/song/{id}", name="admin_song_delete", methods={"DELETE"})
     * @IsGranted("DELETE", subject="song")
     */
    public function delete(Request $request, Song $song): Response
    {
        if ($this->isCsrfTokenValid('delete' . $song->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($song);
            $entityManager->flush();

            $this->addFlash('success', $song->getTitle() . ' has been deleted!');
        }

        return $this->redirectToRoute('admin_song_list');
    }

    /**
     * @Route("/admin/song/{id}/isPublished", name="admin_song_toggle", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function togglePublishedSong(EntityManagerInterface $em, Request $request, SongRepository $sr, Security $security)
    {
        // Is it an Ajax Request ?
        if (!$request->isXmlHttpRequest())
            return new JsonResponse(array('status' => 'Error'), 400);

        // Request has request data ?
        if (!isset($request->request))
            return new JsonResponse(array('status' => 'Error'), 400);

        // Get data
        $checked = intval($request->request->get('checked'));
        $song_id = intval($request->request->get('song_id'));

        // Is the data correct ?
        if ($checked != 1 && $checked != 2)
            return new JsonResponse(array('status' => 'Error'), 400);

        // Does the cookie object exist ?
        $song = $sr->findOneById($song_id);

        if ($song === null)
            return new JsonResponse(array('status' => 'Error'), 400);

        // Does the user have permission to eat the cookie ?
        if (!$security->isGranted('ROLE_ADMIN', $song))
            return new JsonResponse(array('status' => 'Error'), 403);

        if ($song->isPublished() == null) {
            $song->setPublishedAt(new \DateTime());
        } else {
            $song->setPublishedAt(null);
        }
        $em->flush();

        return new JsonResponse(array('status' => 'Done'), 200);
    }

    /**
     * @Route("/admin/song/{slug}/cut", name="mp3_cut_song")
     * @IsGranted("CUT", subject="song")
     */
    public function cutMP3(Song $song)
    {
        /**@var Song */
        $slug  = $song->getSlug();
        $audio = $song->getAudioFileName();
        $title = $song->getTitle();
        $artist = $song->getArtist();
        $pathName = 'C:\\wamp64\\www\\kara_n_blind\\public\\audio\\' . $audio;
        /**@var MpegAudio */
        $mp3 = MpegAudio::fromFile($pathName);
        $extractDuration = $mp3->trim(0, 20);
        $rt = $extractDuration->stripTags();
        $rt->saveFile($this->setPath($title, $artist));
        $path = $this->setPath($title, $artist);
        new File($path);

        return $this->redirectToRoute("admin_song_edit", [
            'slug' => $slug
        ]);
    }

    private function setPath($title, $artist)
    {
        $path = 'C:\\wamp64\\www\\kara_n_blind\\public\\audio\\cut_' . $artist . ' - ' . $title . '.mp3';

        return $path;
    }
}
