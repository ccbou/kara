<?php

namespace App\Controller;

use App\Entity\Song;
use App\Repository\GenreRepository;
use App\Repository\PlaylistRepository;
use App\Repository\SongRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlindtestController extends AbstractController
{
    /**
     * @Route("/blindtest", name="blindtest")
     */
    public function index(GenreRepository $gr, PlaylistRepository $pr)
    {
        $user = $this->getUser();
        $userPlaylists = $pr->findPlaylistsByUser($user);

        $genres = $gr->findAll();

        return $this->render('blindtest/index.html.twig', [
            'userPlaylists' => $userPlaylists,
            'genres' => $genres,

        ]);
    }

    /**
     * @Route("/blindtest/new", name="blindtest_random")
     */
    public function randomBlindtest(SongRepository $sr)
    {
        /**@var Song[] $songs */
        $songs = $sr->findRandomBlindtestSongs();
        // $datas = [];

        // foreach ($songs as $key => $song) {
        //     $datas[$key]['name'] = $song->getTitle();
        //     $datas[$key]['artist'] = $song->getArtist();
        //     $datas[$key]['url'] = '/audio/' . $song->getAudioFileName();
        //     $datas[$key]['cover_art_url'] = '/images/songs/' . $song->getImageFileName();
        // }

        return $this->render('blindtest/blindtest_player.html.twig', [
            'songs' => $songs
        ]);
    }

    /**
     * @Route("blindtest/new/playlist", name="blindtest_new_playlist")
     */
    public function newUserPlaylistBlindtest()
    {
        return new Response('Yeah yeah ouh ouh!');
    }

    /**
     * @Route("/blindtest/new/genre", name="blindtest_new_genre")
     */
    public function newGenreBlindtest()
    {
        return new Response('Yeah yeah ouh ouh!');
    }
}
