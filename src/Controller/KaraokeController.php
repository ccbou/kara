<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Entity\Song;
use App\Entity\Playlist;
use App\Service\MediaManager;
use App\Repository\SongRepository;
use App\Repository\GenreRepository;
use App\Repository\PlaylistRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class KaraokeController extends AbstractController
{
    /**
     * @Route("/karaoke", name="karaoke")
     */
    public function index(PlaylistRepository $pr, GenreRepository $gr)
    {
        $user = $this->getUser();
        $userPlaylists = $pr->findPlaylistsByUser($user);

        $genres = $gr->findAll();

        return $this->render('karaoke/index.html.twig', [
            'userPlaylists' => $userPlaylists,
            'genres' => $genres,
        ]);
    }

    /**
     * @Route("/karaoke/new/playlist/{slug}", name="karaoke_new_playlist")
     * @IsGranted("KARAOKE_PLAYLIST", subject="playlist")
     */
    public function newUserPlaylistKaraoke(Playlist $playlist, MediaManager $mediaManager, PlaylistRepository $pr)
    {
        $playlistSongs = $playlist->getSongs()->toArray();
        $randomPlaylistSong = $mediaManager->arrayRandom($playlistSongs);
        $embedLink = $mediaManager->getEmbedVideoLink($randomPlaylistSong);

        return $this->render('partials/_show_song.html.twig', [
            'song' => $randomPlaylistSong,
            'embedLink' => $embedLink,
            'name' => 'newKaraokePlaylist',
            'playlist' => $playlist
        ]);
    }

    /**
     * @Route("/karaoke/new/genre/{slug}", name="karaoke_new_genre")
     * @IsGranted("ROLE_USER")
     */
    public function newGenreKaraoke(Genre $genre, MediaManager $mediaManager)
    {
        $genreSongs = $genre->getSongs()->toArray();
        $randomGenreSong = $mediaManager->arrayRandom($genreSongs);
        $embedLink = $mediaManager->getEmbedVideoLink($randomGenreSong);

        return $this->render('partials/_show_song.html.twig', [
            'song' => $randomGenreSong,
            'embedLink' => $embedLink,
            'genre' => $genre,
            'name' => 'newKaraokeGenre'
        ]);
    }

    /**
     * @Route("/karaoke/new", name="karaoke_random")
     */
    public function randomKaraoke(SongRepository $sr, MediaManager $mediaManager)
    {
        $randomSong = $sr->findRandomSong();
        $embedLink = $mediaManager->getEmbedVideoLink($randomSong);

        return $this->render('partials/_show_song.html.twig', [
            'embedLink' => $embedLink,
            'song' => $randomSong,
            'name' => 'newKaraokeRandom',
        ]);
    }
}
