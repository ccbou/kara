<?php

namespace App\Controller;

use App\Repository\SongRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(SongRepository $repo)
    {
        $songs = $repo->findHomepageRandomSongs();
        $lastSongs = $repo->findTenLatestSongs();

        return $this->render('home/index.html.twig', [
            'songs' => $songs,
            'lastSongs' => $lastSongs,
        ]);
    }
}
