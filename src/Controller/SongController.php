<?php

namespace App\Controller;

use App\Entity\Song;
use App\Form\SongFormType;
use App\Service\MediaManager;
use App\Form\PlaylistSongFormType;
use App\Repository\SongRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class SongController extends AbstractController
{
    /**
     * @Route("/song/list", name="song_list")
     */
    public function list(SongRepository $repo, PaginationService $paginationService)
    {
        $datas = $repo->findAllSongsPublishedByAlphabet();
        $songs = $paginationService->pagination($datas, 12, 1);

        return $this->render('song/list.html.twig', [
            'songs' => $songs,
        ]);
    }

    /**
     * @Route("/song/list/{artist}", name="song_list_by_artist")
     */
    public function listByArtist(SongRepository $sr, $artist, PaginationService $paginationService)
    {
        $datas = $sr->findAllSongsByArtist($artist);
        $songs = $paginationService->pagination($datas, 12, 1);

        return $this->render('song/list.html.twig', [
            'songs' => $songs
        ]);
    }

    /**
     * @Route("/song/list/genre/{genre}", name="song_list_by_genre")
     */
    public function listByGenre(SongRepository $sr, PaginationService $paginationService, $genre)
    {
        $datas = $sr->findAllSongsByGenre($genre);
        $songs = $paginationService->pagination($datas, 12, 1);

        return $this->render('song/list.html.twig', [
            'songs' => $songs
        ]);
    }

    /**
     * @Route("/song/new", name="song_new")
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request, EntityManagerInterface $em, Security $security)
    {
        $form = $this->createForm(SongFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**@var Song $song */
            $song = $form->getData();
            $song->setCreatedAt(new \DateTime());
            $song->setUser($this->getUser());

            if ($security->isGranted('ROLE_ADMIN')) {
                $song->setPublishedAt(new \DateTime());
            }

            $em->persist($song);
            $em->flush();

            if ($security->isGranted("ROLE_ADMIN")) {
                $this->addFlash('success', $song->getTitle() . ' has been added and published!');
                return $this->redirectToRoute('admin_song_list');
            } else {
                $this->addFlash('success', 'Your song has been added, our moderators will check the informations and publish it.');
                return $this->redirectToRoute('user_song_list');
            }
        }

        return $this->render('song/new.html.twig', [
            'songForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/song/{id}/playlist/new", name="song_playlist_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function newSongPlaylist(Request $request, EntityManagerInterface $em, Song $song)
    {
        $form = $this->createForm(PlaylistSongFormType::class, $song);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $songPlaylist = $form->getData();
            $em->persist($songPlaylist);
            $em->flush();
        }

        return $this->render('song/_form_playlist_song.html.twig', [
            'form' => $form->createView(),
            'song' => $song,
        ]);
    }

    /**
     * @Route("/song/{slug}", name="song_show")
     */
    public function show($slug, SongRepository $repo, MediaManager $mediaManager)
    {
        /**@var Song $song */
        $song = $repo->findOneBy(['slug' => $slug]);
        if (!$song) {
            throw $this->createNotFoundException(sprintf('No song for slug "%s"', $slug));
        }

        $embedLink = $mediaManager->getEmbedVideoLink($song);

        return $this->render('partials/_show_song.html.twig', [
            'song' => $song,
            'embedLink' => $embedLink,
            'name' => 'showSong'
        ]);
    }

    /**
     * @Route("/song/{slug}/heart", name="song_toggle_heart", methods={"POST"})
     * @IsGranted("ROLE_USER")
     */
    public function toggleSongHeart(Song $song, EntityManagerInterface $em, MediaManager $mediaManager)
    {
        // TODO - actually heart/unheart the article!
        $mediaManager->incrementHeartCount($song, $em);

        return new JsonResponse(['hearts' => $song->getHeartCount()]);
    }
}
