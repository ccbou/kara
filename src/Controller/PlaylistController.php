<?php

namespace App\Controller;

use App\Entity\Playlist;
use App\Entity\Song;
use App\Form\PlaylistFormType;
use App\Repository\PlaylistRepository;
use App\Repository\SongRepository;
use App\Repository\UserRepository;
use App\Service\PaginationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/playlist")
 * @IsGranted("ROLE_USER")
 */
class PlaylistController extends AbstractController
{
    /**
     * @Route("/", name="playlist_index", methods={"GET"})
     */
    public function index(UserRepository $ur, PaginationService $paginationService): Response
    {
        $id = $this->getUser()->getId();
        $user = $ur->findOneBy(['id' => $id]);
        $datas = $user->getPlaylists();
        $playlists = $paginationService->pagination($datas, 10, 1);

        return $this->render('playlist/index.html.twig', [
            'playlists' => $playlists,
        ]);
    }

    /**
     * @Route("/new", name="playlist_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playlist = new Playlist();
        $form = $this->createForm(PlaylistFormType::class, $playlist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**@var Playlist $playlist */
            $playlist = $form->getData();
            $playlist->setUser($this->getUser());
            $playlist->setCreatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playlist);
            $entityManager->flush();

            return $this->redirectToRoute('playlist_index');
        }

        return $this->render('playlist/new.html.twig', [
            'playlist' => $playlist,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="playlist_show", methods={"GET"})
     * @IsGranted("SHOW", subject="playlist")
     */
    public function show(Playlist $playlist, PaginationService $ps, PlaylistRepository $pr): Response
    {
        $datas = $playlist->getSongs();
        // $datas = $pr->findSongsFromAPlaylist($playlist);
        // $songs = $playlist->getSongs();
        //dd($datas);
        $count = count($datas);
        $playlistSongs = $ps->pagination($datas, 10, 1);

        return $this->render('playlist/show.html.twig', [
            'playlist' => $playlist,
            'playlistSongs' => $playlistSongs,
            'count' => $count,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="playlist_edit", methods={"GET","POST"})
     * @IsGranted("EDIT", subject="playlist")
     */
    public function edit(Request $request, Playlist $playlist): Response
    {
        $form = $this->createForm(PlaylistFormType::class, $playlist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $playlist->setUpdatedAt(new \DateTime());

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Playlist: ' . $playlist->getName() . 'has been updated.');

            return $this->redirectToRoute('playlist_index');
        }

        return $this->render('playlist/edit.html.twig', [
            'playlist' => $playlist,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="playlist_delete", methods={"DELETE"})
     * @IsGranted("DELETE", subject="playlist")
     */
    public function delete(Request $request, Playlist $playlist): Response
    {
        if ($this->isCsrfTokenValid('delete' . $playlist->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playlist);
            $entityManager->flush();
        }

        return $this->redirectToRoute('playlist_index');
    }

    /**
     * @Route("/{playlist_id}/{song_id}/remove", name="remove_song_from_playlist")
     */
    public function removeSongFromPlaylist($playlist_id, $song_id, EntityManagerInterface $em, PlaylistRepository $pr, SongRepository $sr)
    {
        $playlist = $pr->find($playlist_id);
        if (!$playlist_id) {
            throw $this->createNotFoundException('Playlist Not Found');
        }
        $this->denyAccessUnlessGranted('REMOVE_SONG_FROM_PLAYLIST', $playlist);

        $song = $sr->find($song_id);
        if (!$song) {
            throw $this->createNotFoundException('Song Not Found');
        }

        $playlist->removeSong($song);
        $em->persist($playlist);
        $em->flush();

        // return $this->redirectToRoute('playlist_show', [
        //     'id' => $playlist->getId()
        // ]);
        return new Response(null, 204);
    }
}
