<?php

namespace App\Controller;

use App\Controller\BaseController;
use App\Entity\User;
use App\Repository\SongRepository;
use App\Repository\UserRepository;
use App\Service\MediaManager;
use App\Service\PaginationService;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AccountController extends BaseController
{
    /**
     * @Route("/account", name="app_account")
     */
    public function index(MediaManager $mediaManager)
    {
        $file = $this->getUser()->getImageFileName();
        $avatarPath = $mediaManager->getAvatarPath($file);


        return $this->render('account/index.html.twig', [
            'avatarPath' => $avatarPath,
        ]);
    }

    /**
     * @Route("/account/mysongs", name="user_song_list")
     */
    public function userSongList(UserRepository $ur, PaginationService $paginationService)
    {
        $id = $this->getUser()->getId();
        $userId = $ur->findOneBy(['id' => $id]);
        $datas = $userId->getSongs();
        $songs = $paginationService->pagination($datas, 10, 1);

        return $this->render('account/user_song_list.html.twig', [
            'songs' => $songs,
        ]);
    }
}
