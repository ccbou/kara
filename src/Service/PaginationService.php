<?php

namespace App\Service;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PaginationService
{
    private $paginator;
    private $requestStack;

    public function __construct(PaginatorInterface $paginator, RequestStack $requestStack)
    {
        $this->paginator = $paginator;
        $this->requestStack = $requestStack;
    }

    public function pagination($datas, $numberOfItems, $pageNumber)
    {
        $request = $this->requestStack->getCurrentRequest();

        $pagination = $this->paginator->paginate(
            $datas,
            $request->query->get('page', $pageNumber),
            $numberOfItems
        );

        return $pagination;
    }
}
