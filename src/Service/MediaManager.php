<?php

namespace App\Service;

use App\Entity\Song;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class MediaManager
{

    // not used for the moment
    public function getImagePath(string $path = null)
    {
        if ($path = null) {
            $fileName = 'neon-direction-fleche-perspective_1017-22033.jpg';
        } else {
            $fileName = $path;
        }

        return 'images/' . $fileName;
    }

    /**
     * @return string $embedVideoLink
     */
    public function getEmbedVideoLink(Song $song)
    {
        $link = $song->getVideoLink();
        if ($link !== null) {
            $embedVideoLink = str_replace('www.youtube.com/watch?v=', 'www.youtube.com/embed/', $link);
            if (strpos($embedVideoLink, 'list') == true) {
                $embedVideoLink = strstr($embedVideoLink, '&', true);
            }
        } else {
            return null;
        }
        return $embedVideoLink;
    }

    public function incrementHeartCount(Song $song, EntityManagerInterface $em)
    {
        $song->setHeartCount($song->getHeartCount() + 1);
        $em->flush();
    }

    public function getAvatarPath(string $fileName = null)
    {
        if (!$fileName) {
            $fileName = 'neon-direction-fleche-perspective_1017-22033.jpg';
        }

        return 'images/' . $fileName;
    }

    // Get one random row in an array
    public function arrayRandom($array, $num = 1)
    {
        shuffle($array);
        $r = [];
        for ($i = 0; $i < $num; $i++) {
            $r[] = $array[$i];
        }

        return $num == 1 ? $r[0] : $r;
    }
}
