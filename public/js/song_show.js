$(document).ready(function () {
    $('.js-like-song').on('click', function (e) {
        e.preventDefault();
        let $link = $(e.currentTarget);
        $link.toggleClass('fa-heart-o').toggleClass('fa-heart');

        $.ajax({
            method: 'POST',
            url: $link.attr('href')
        }).done(function (data) {
            $('.js-like-song-count').html(data.hearts);
        })
    });
});