function flashMessage() {
    (function ($) {
        $.fn.flash_message = function (options) {

            options = $.extend({
                text: 'Done',
                time: 1000,
                how: 'before',
                class_name: ''
            }, options);
            console.log(options);

            return $(this).each(function () {
                if ($(this).parent().find('.flash_message').get(0))
                    return;

                var message = $('<span />', {
                    'class': 'flash_message ' + options.class_name,
                    text: options.text
                }).hide().fadeIn('fast');

                $(this)[options.how](message);

                message.delay(options.time).fadeOut('normal', function () {
                    $(this).remove();
                });

            });
        };
    })(jQuery);

    $('.js-playlist-song-input').change(function () {
        console.log('Yeah yeah ouh ouh!!');

        $('#status-area').flash_message({
            text: 'Added to cart!',
            how: 'append'
        });
    });
}