$(document).ready(function () {
    let path = "/karaoke/new";
    $("#karaoke_launcher_button").attr("href", path);
    $(document).on("change", ".js-selection", function () {
        $("#karaoke_launcher_button").attr("href", $(this).val());
    });
});

$(document).ready(function () {
    let path = "/blindtest/new";
    $("#blindtest_launcher_button").attr("href", path);
    $(document).on("change", ".js-selection", function () {
        $("#blindtest_launcher_button").attr("href", $(this).val());
    });
});

function hideKaraokeElement(div, or, element) {
    document.getElementById(div).style.display = (element.value != "/karaoke/new") ? 'none' : 'block';
    document.getElementById(or).style.display = element.value != "/karaoke/new" ? 'none' : 'block';
}

function hideBlindtestElement(div, or, element) {
    document.getElementById(div).style.display = element.value != "/blindtest/new" ? 'none' : 'block';
    document.getElementById(or).style.display = element.value != "/blindtest/new" ? 'none' : 'block';
}