// OPEN THE MODAL WITH FORM PLAYLISTSONG
$(document).ready(function () {
    $(".modal-trigger").click(function (e) {
        e.preventDefault();
        $(".modal").modal();
        let url = $(this).attr("data-path");

        $.get(url, function (data) {
            $(".modal-body").html(data);
            $("#playlistModal").modal();

            $(".modal-body").ready(function () {
                $("#js-playlist-song-form").submit(function () {
                    // ENVOI AJAX 
                    $.ajax({
                        type: "POST",
                        url: $(this).attr("action"),
                        dataType: "html",
                        data: $("#js-playlist-song-form").serialize()
                    });
                    return false;
                });
                $('input[type=checkbox]').change(function () {
                    $("#js-playlist-song-form").submit();
                });
            });
        });
    });
});