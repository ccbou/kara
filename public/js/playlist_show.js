$(document).ready(function () {
    $('.js-remove-song-from-playlist').on('click', function (e) {
        e.preventDefault();
        let $el = $(this);
        $(this).find('.fa-trash')
            .removeClass('fa-close')
            .addClass('fa-spinner')
            .addClass('fa-spin')

        $.ajax({
            url: $(this).data('url'),
            method: 'DELETE'
        }).done(function () {
            $el.fadeOut();
        });
    })
});